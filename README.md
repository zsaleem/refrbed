## Requirements
Below is the stack I used in this project. Please follow the getting started section in order to make this project run.

## Stack
 - CSS3
 - SCSS
 - BEM
 - CSS Animations
 - CSS Media Queries
 - Mobile first responsive
 - Git
 - Gitflow
 - Gitlab
 - NPM/Yarn
 - Netlify
 - Sublime Text
 - Mac OS X
 - Google dev tool lighthouse
 - Google Chrome
 - Safari
 - Firfox
 - Opera
 - Webpack
 - Babel
 - eslint
 - nodejs(16.9.1)

## Getting started
In order to run this project in the browser successfully, please follow the steps below!

    1. Clone this repository.
    2. cd into `/refrbed` directory.
    3. Run `npm install or yarn` command to download and install all dependencies.
    4. To run this project use `yarn serve or npm run serve` command in command line.
    5. To build the project for production run `yarn build:prod or npm run build:prod` command. This will build the app for production and put all the files in `/dist` folder.
    6. To run production ready version on local environment, run `yarn serve:prod or npm run serve:prod`. Then go to `http://localhost:8080`.

## Description
Above steps, in getting started section, will install all the dependencies required for this project to run.

For this project, I tried to fork the codepen.io link however, it was not forking the link. I was getting below message for a couple of hours.

![Screenshot](screenshots/message.png)

So I went on and setup the whole development environment from scratch using `webpack` and `babel`. With this setup, I took advantage to show off other skills related to front end development that were not mentioned in coding tasl i.e. setting up development environment, using webpack, babel, setting up CI/CD processes, using git, gitlab and production servers etc etc.

In this task I used `SCSS/SASS`. I developed the entire project inside `/refrbed` folder. All the styles are written inside `styles/sass/card.scss` file.

The entire html for this project is inside `/src/index.html`.

I also created a video from this project which shows the app in action. [Click here](https://youtu.be/SRNZhN0CZNQ) to watch it in action.

You can also view this task on live server [here](https://refrbed.netlify.app/)

### Development Environments
As I mentioned I developed the whole development environment for this project. In order to achieve that I used `Webpack`. I wrote the `webpack.config.js` file from scratch. What this file basically does that it checks the current `mode` of the running server. If it is `development` then load the development specific web pack file which is `/build/development.js` file. If the `mode` is `production` then load production specific file which is `/build/production.js`.

For `development` environment, there is no need to build and minify all the resources simply go ahead and execute the project in local environment as is.

For `production` environment, I am adding a tarser plugin to minify all the resources for production release.

The `/build/common.js` file is basically contains the common code for both `development` and `production` environment with minor changes just to follow DRY practices.

### SEO & Accessibility
To make sure the SEO and Accessibility is at 100%, I used lighthouse tool. Below are the images for the result.

For desktop version
![Screenshot](screenshots/desktop.png)

For Mobile version
![Screenshot](screenshots/mobile.png)

### Architecture
For this task I followed the BEM Methodology. Since I it was instructed to specifically use `HTML and CSS` for this task so I had no options in choosing modern languages with modern frameworks. So I wrote all the css/sass in `/src/styles/` folder. Even though it was instrcuted in the task description that I use `CSS` however, in codepen link I saw there was also `scss` files from that I assumed that `scss` is also allowed to use and I utilised it.

### Performance
In terms of performance, as I mentioned I am using webpack to build the project for production. So it minifies all the resources which reduces its files sizes which loads faster on the network.

### Development process
The development process that I followed for this project is `Gitflow` work flow. That means, I have two git branches `master` and `develop`. So I created a `feature branch` from `develop` branch. I develop the whole feature in that branch. Once I am done with it. I `merge` that branch with `develop` branch and push it to remote repo with `develop` branch. When I need to release new feature, I create a new branch `release` with a version number on it. Then I merge that `release` branch into master branch and push it to remote repo's `master` branch.

### Code Quality
I wrote the entire project in components architecture which means everything is a component. I wrote all the code very readable and easy to follow. In addition, I used `eslint` to keep the quality of the code intact.

## Notes
If you run the project locally and look it into browser you will notice there are two cards stacked. The top version is exactly the like the design provided to me. The problem with this was is accessibility. It was returning low percentage in terms of accessiblity. But I kept it as the design is so that I do not lose any points.

The second version a little different i.e. the text color in the badge is black which gives higher contrast ratio because of its background color which improves the accessibility. Second change is the color of `9999.99` and `Gratis Versand, inkl. MwSt.`. They are also black in color and black color gives hight contrast ratio against its background color which is white and thus improves the accessibility percentage.

